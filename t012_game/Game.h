#pragma once

using namespace sf;

//dimensions in 2D that are whole numbers
struct Dim2Di
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
	float x, y;
};

/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width,
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Dim2Di SCREEN_RES{ 800,600 };

	const char ESCAPE_KEY{ 27 };
	
	//Ship constants
	const float SHIP_RADIUS = 25.f; //radius of ship

	//Rock constants
	const Dim2Df ROCK_RAD{ 10.f, 40.f }; //size of rocks, between 10 and 40
	const float ROCK_MIN_DIST = 2.5f; //stop them getting too close
	const int NUM_ROCKS = 50; //how many rocks to place
	const float ROCK_SPEED = 150.f; //speed at which rocks move
	const int PLACE_TRIES = 10; //how many times to try and place

	//Bullet constants
	const int NUM_BULLETS = 50; //how many bullets to place
}

struct Object 
{
	//Variables
	enum class ObjT {Ship, Rock, Bullet};
	ObjT type = ObjT::Ship;
	Sprite spr;
	float radius = 0.f; //radius of objects
	bool colliding = false; //check if objects are colldiing
	bool active = true; //check if objects should be set active or not
	int health = 1; //when 0, object is set to be inactive

	//Initialisation
	void Init(RenderWindow&, Texture&, ObjT);

	void InitShip(RenderWindow&, Texture&);
	void InitRock(RenderWindow&, Texture&);
	void InitBullet(RenderWindow&, Texture&);

	//Update
	void Update(RenderWindow&, float, std::vector<Object>&, bool);

	void PlayerControl(const Vector2u&, float, std::vector<Object>&, bool);

	void RotateRock();
	void MoveRock(float);

	void FireBullet(const Vector2f&, std::vector<Object>&);
	void MoveBullet(const Vector2u&, float);

	void Hit(Object&);
	void TakeDamage(int);

	//Render
	void Render(RenderWindow&);
};

void PlaceRocks(RenderWindow&, Texture&, std::vector<Object>&);

bool LoadTexture(const std::string&, Texture&);

void CheckCollisions(std::vector<Object>&, RenderWindow&, bool debug = true);

bool CircleToCircle(const Vector2f&, const Vector2f&, float);

void DrawCircle(RenderWindow&, const Vector2f&, float, Color);

bool IsColliding(Object&, std::vector<Object>&);

bool SpawnRock(RenderWindow&, std::vector<Object>&, float);

Vector2f Decay(Vector2f&, float, float, float);